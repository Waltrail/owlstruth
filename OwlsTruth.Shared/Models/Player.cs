﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OwlsTruth
{
    public class Player : Sprite
    {
        public float JumpSpeed;
        protected bool OnGround;
        private bool jump;
        private float FallTime = 0;
        private float FallSpeed = 0.01f;
        public float Speed;
        private float timer = 0;
        private float Time;
        private bool rare;
        public bool isControllable;
        private float LastYpos;
        public Rectangle SceneBox;
        private Vector2 PrefPosition;
        private Vector2 SavePosition;

        public Player(Texture2D texture) : base(texture)
        {
            Speed = 0.125f;
            Time = 300;
            JumpSpeed = 0.075f;
            LastYpos = 0;
            BoxFix = 8;
            rare = false;
            SceneBox = new Rectangle();
            PrefPosition = new Vector2();
            isControllable = true;
        }

        public Player(Dictionary<string, Animation> _animations) : base(_animations)
        {
            Speed = 0.125f;
            Time = 300;
            JumpSpeed = 0.075f;
            LastYpos = 0;
            BoxFix = 8;
            rare = false;
            SceneBox = new Rectangle();
            PrefPosition = new Vector2();
            isControllable = true;
        }        

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            BoxFix = 8;
            PrefPosition = position;
            Move();            
            OnGround = false;
            if (Time == 300)
            {
                Time = OwlsTruth.random.Next(10, 25);
            }            
            foreach (var sprite in sprites)
            {
                if (sprite == this)
                {
                    continue;
                }
                if (IsTouchingBottom(sprite))
                {
                    OnGround = true;
                    LastYpos = sprite.Rectangle.Top - Rectangle.Height;
                    if(!(sprite is Tile))
                        SavePosition = new Vector2(PrefPosition.X, LastYpos);
                }
                if (IsTouchingLeft(sprite) || IsTouchingRight(sprite))
                {
                    Velocity.X = 0;
                }
            }
            Jump();
            if (OnGround)
            {
                Velocity.Y = 0;
                FallTime = 0;
                position.Y = LastYpos;
            }
            else
            {                
                FallTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if(Velocity.Y < 0.3f || FallSpeed < 0 || (Velocity.Y > -0.4 && Velocity.Y < 0))
                    Velocity.Y += FallSpeed;
                if(FallSpeed < 0.04f)
                {
                    FallSpeed += 0.01f * FallTime;
                }
                if(FallTime - 1 > 1.1f && Velocity.Y >= 0.29f)
                {
                    Die();
                }
            }
            if (!isControllable)
            {
                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timer > 1)
                {
                    timer = 0;
                    position = SavePosition;
                    Velocity = Vector2.Zero;
                    isControllable = true;
                }
            }            
            position += Velocity * gameTime.ElapsedGameTime.Milliseconds;
            
            position.X = MathHelper.Clamp(position.X, SceneBox.Left, SceneBox.Right);
            SetAnimation();
            if (animations != null)
            {
                animationManager.Update(gameTime);
                animationManager.Position = Position;
            }            

            base.Update(gameTime, sprites);
        }

        private void Die()
        {
            isControllable = false;            
        }

        protected override void SetAnimation()
        {
            if (animations != null)
            {
                if (jump)
                {
                    animationManager.Play(animations[key: "jump"]);
                    if (animationManager._animation.CurrentFrame == animationManager._animation.FrameCount - 1)
                    {
                        jump = false;
                    }
                }
                else if (Velocity.Y != 0)
                {
                    animationManager.Play(animations[key: "air"]);
                    if (Velocity.X > 0)
                    {
                        animationManager.Flip = true;
                    }
                    else if (Velocity.X < 0)
                    {
                        animationManager.Flip = false;
                    }
                }
                else if (Velocity.X > 0)
                {
                    rare = false;
                    animationManager.Flip = true;
                    animationManager.Play(animations[key: "run"]);
                }
                else if (Velocity.X < 0)
                {
                    rare = false;
                    animationManager.Flip = false;
                    animationManager.Play(animations[key: "run"]);
                }
                else if (Velocity.X == 0)
                {                    
                    if (rare)
                    {
                        animationManager.Play(animations[key: "rare"]);
                        rare = true;
                        if (animationManager._animation.CurrentFrame >= animationManager._animation.FrameCount - 1)
                        {
                            rare = false;
                            Time = 300;
                        }
                    }
                    else
                    {
                        animationManager.Play(animations[key: "idle"]);
                    }
                }
            }
        }
        private void Move()
        {
            if (isControllable)
            {
                if (InputManager.Left)
                {
                    Velocity.X = -Speed;
                }
                else if (InputManager.Right)
                {
                    Velocity.X = Speed;
                }
                else if (OnGround)
                {
                    Velocity.X = 0;
                }
            }
        }
        private void Jump()
        {
            if (InputManager.Up && OnGround && isControllable)
            {
                FallSpeed = -JumpSpeed;
                FallTime = 1f;
                OnGround = false;
                jump = true;
            }
        }
    }
}
