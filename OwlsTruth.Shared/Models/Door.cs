﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace OwlsTruth
{
    public class Door
    {
        public Vector2 Position;
        public bool isOpened;

        private bool isOpening;
        private Texture2D closed;
        private Texture2D opened;
        private AnimationManager opening;
        private Action state;
        private Rectangle rectangle { get
            {
                return new Rectangle((int)Position.X, (int)Position.Y, closed.Width, closed.Height);
            }
        }

        public Door(Texture2D opened, Action State, Texture2D closed = null, Animation opening = null)
        {
            this.opening = new AnimationManager(opening);            
            this.closed = closed;
            this.opened = opened;
            state = State;
            isOpened = false;
            isOpening = false;
        }
        public void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (opening != null && !isOpened && isOpening)
            {
                opening.Position = Position;
                opening.Update(gameTime);
            }
            foreach (var sprite in sprites)
            {
                if (sprite is Player)
                {
                    if (sprite.Rectangle.Intersects(rectangle))
                    {
                        if (InputManager.Up && isOpened)
                        {
                            state();
                        }
                        else if (InputManager.Up && !isOpened)
                        {
                            isOpening = true;
                            opening.Play(opening._animation);
                            if(opening._animation.CurrentFrame == opening._animation.FrameCount - 1)
                            {                                
                                isOpened = true;
                            }
                        }
                    }
                }
            }           
            
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (opening._animation.CurrentFrame != 0 && !isOpened && isOpening)
            {
                opening.Draw(spriteBatch);
            }
            else if (isOpened)
            {
                spriteBatch.Draw(opened, Position, Color.White);
            }
            else if (!isOpened)
            {
                spriteBatch.Draw(closed, Position, Color.White);
            }
        }
    }
}
