﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OwlsTruth
{
    public class Sprite
    {
        protected Texture2D rectex;
        protected Dictionary<string, Animation> animations;
        protected Vector2 position;

        public Vector2 Velocity;
        public bool State;
        public int BoxFix;
        public Color color;
        public Texture2D _texture;
        public AnimationManager animationManager;        
        
        public Vector2 Origin
        {
            get
            {
                return new Vector2(Position.X + Rectangle.Width / 2, Position.Y + Rectangle.Height);
            }
        }
        public virtual Rectangle Rectangle
        {
            get
            {
                if (_texture != null)
                {
                    return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
                }
                if (animations != null)
                {
                    if (animationManager._animation == null)
                    {
                        return new Rectangle((int)position.X + BoxFix, (int)position.Y, animations.First().Value.Texture.Width / animations.First().Value.FrameCount - (BoxFix * 2), animations.First().Value.Texture.Height);
                    }
                    else
                    {
                        return new Rectangle((int)position.X + BoxFix, (int)position.Y, animationManager._animation.Texture.Width / animationManager._animation.FrameCount - (BoxFix * 2), animationManager._animation.Texture.Height);
                    }
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
            }
        }
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                if (animationManager != null)
                {
                    animationManager.Position = position;
                }
            }
        }

        public Sprite(Texture2D texture)
        {
            _texture = texture;
            BoxFix = 0;
            color = Color.White;
        }
        public Sprite(Dictionary<string, Animation> _animations)
        {
            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value);            
            color = Color.White;
        }
        public virtual void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (animations != null)
            {
                SetAnimation();
                animationManager.Update(gameTime);
            }
        }

        protected virtual void SetAnimation()
        {
            animationManager.Play(animations.First().Value);
        }
        
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position, color);
            }
            else if (animationManager != null)
            {
                animationManager.Draw(spriteBatch);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }            
        }        

        protected bool IsTouchingLeft(Sprite sprite)
        {
            return this.Rectangle.Left + Velocity.X <= sprite.Rectangle.Right &&
                this.Rectangle.Left > sprite.Rectangle.Left &&
                this.Rectangle.Top < sprite.Rectangle.Bottom &&
                this.Rectangle.Bottom > sprite.Rectangle.Top;
        }
        protected bool IsTouchingRight(Sprite sprite)
        {
            return this.Rectangle.Right +  Velocity.X >= sprite.Rectangle.Left &&
                this.Rectangle.Right < sprite.Rectangle.Right &&
                this.Rectangle.Top < sprite.Rectangle.Bottom &&
                this.Rectangle.Bottom > sprite.Rectangle.Top;
        }
        protected bool IsTouchingTop(Sprite sprite)
        {
            return this.Rectangle.Bottom + Velocity.Y > sprite.Rectangle.Top &&
                this.Rectangle.Top < sprite.Rectangle.Top &&
                this.Rectangle.Right > sprite.Rectangle.Left &&
                this.Rectangle.Left < sprite.Rectangle.Right;
        }
        protected bool IsTouchingBottom(Sprite sprite)
        {            
            return this.Rectangle.Bottom + Velocity.Y >= sprite.Rectangle.Top &&
                this.Rectangle.Left < sprite.Rectangle.Right &&
                this.Rectangle.Right > sprite.Rectangle.Left &&
                this.Rectangle.Bottom < sprite.Rectangle.Bottom;
        }
    }
}
