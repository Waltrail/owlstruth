﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace OwlsTruth
{
    class Tile : Sprite
    {
        public float offset;
        public float timeOut;
        
        private float duration;
        private float alpha;
        private float timer1;
        private float timer2;
        private float timeTail;
        private float firstHit;
        private bool isTouchable;
        private bool play;
        

        public Tile(Texture2D texture, float timeOut) : base(texture)
        {
            isTouchable = false;
            play = false;
            timer1 = 0;            
            alpha = 0;
            duration = 2.9f;
            timeTail = 0.2f;
            firstHit = 2.1f;
            this.timeOut = timeOut;
            timer2 = (duration + timeTail) * timeOut;            
        }

        public override Rectangle Rectangle
        {
            get
            {
                if (_texture != null && isTouchable)
                {
                    return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
                }
                else
                {
                    return new Rectangle();
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position, color * alpha);
            }
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (timer1 >= offset * (duration + timeTail))
            {
                timer1 = 0;
                offset = 0;                
                if (isTouchable)
                {
                    if (timer2 >= firstHit && timer2 <= firstHit + 0.3)
                    {
                        alpha = 0.5f;
                    }
                    else
                    {
                        alpha = 1;
                    }
                    if (timer2 >= duration)
                    {
                        timer2 = 0;
                        isTouchable = false;                        
                    }
                    timer2 += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
                else
                {
                    alpha = 0;
                    if(timer2 >= (duration + timeTail)* timeOut)
                    {
                        Console.WriteLine(timer2);
                        Console.WriteLine(timeOut);
                        timer2 = 0;
                        isTouchable = true;
                        play = true;
                        alpha = 1;
                    }
                    timer2 += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
            }
            if(offset != 0)
                timer1 += (float)gameTime.ElapsedGameTime.TotalSeconds;
        }   
        public void PlaySound(Vector2 playerPosition)
        {
            if (!play)
                return;

            play = false;            
            SoundEffectInstance instance = SoundManager.tile.CreateInstance();
            AudioEmitter emitter = new AudioEmitter();
            AudioListener listener = new AudioListener();
            listener.Position = new Vector3(playerPosition, 0);
            emitter.Position = new Vector3(Rectangle.Center.ToVector2(), 0);
            instance.Play();
        }        

        public decimal Map(decimal value, decimal fromSource, decimal toSource, decimal fromTarget, decimal toTarget)
        {
            return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
        }
    }
}
