﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace OwlsTruth
{
    public class AnimationManager
    {
        private float timer;

        public Animation _animation;        
        public Color Color;
        public Point Scale;

        public bool Flip { get; set; }
        public Vector2 Position { get; set; }
        

        public Rectangle destinationRectangle { get
            {
                return new Rectangle(Position.ToPoint(), Scale);
            }
        }

        public AnimationManager(Animation animation)
        {
            _animation = animation;
            Color = Color.White;
            Scale = new Point(animation.FrameWight, animation.FrameHight);
        }

        public void Play(Animation animation)
        {
            Scale = new Point(animation.FrameWight, animation.FrameHight);
            if (_animation == animation)
            {
                return;
            }
            _animation = animation;
            _animation.CurrentFrame = 0;
            timer = 0f;
        }
        public void Stop()
        {
            timer = 0;
            _animation.CurrentFrame = 0;
        }
        public void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (timer > _animation.Speed)
            {
                timer = 0f;
                _animation.CurrentFrame++;
                if (_animation.CurrentFrame >= _animation.FrameCount && _animation.IsLooping)
                {
                    _animation.CurrentFrame = 0;
                }
                if (_animation.CurrentFrame >= _animation.FrameCount && !_animation.IsLooping)
                {
                    _animation.CurrentFrame = _animation.FrameCount - 1;
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {            
            if (!Flip)
            {
                
                spriteBatch.Draw(_animation.Texture,
                                 destinationRectangle,
                                 new Rectangle(_animation.CurrentFrame * _animation.FrameWight, 0, _animation.FrameWight, _animation.FrameHight),
                                 Color);                
            }
            else
            {
                spriteBatch.Draw(_animation.Texture,
                                 destinationRectangle,
                                 new Rectangle(_animation.CurrentFrame * _animation.FrameWight, 0, _animation.FrameWight, _animation.FrameHight),
                                 Color, 0 , Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
            }
        }
    }
}
