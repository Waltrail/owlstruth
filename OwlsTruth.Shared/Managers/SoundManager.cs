﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace OwlsTruth
{
    public static class SoundManager
    {
        public static float Volume;
        public static float MusicVolume;
        public static float SoundEffectsVolume;
        public static Dictionary<string, Song> songs;
        public static SoundEffect tile;
        private static State state;

        public static void Load(ContentManager content)
        {
            songs = new Dictionary<string, Song>()
            {
                {"beginning" ,content.Load<Song>("Sounds/beginning")},
                {"owls" ,content.Load<Song>("Sounds/OwlsTruth")}
            };
            tile = content.Load<SoundEffect>("Sounds/tile");
            Volume = 0.5f;
            MusicVolume = 1;
            SoundEffectsVolume = 1;
            MediaPlayer.Volume = MusicVolume * Volume;
            SoundEffect.MasterVolume = SoundEffectsVolume * Volume;
            MediaPlayer.IsRepeating = true;
        }
        public static void Update(State state)
        {
            SoundManager.state = state;
            MediaPlayer.Volume = MusicVolume * Volume;
            SoundEffect.MasterVolume = SoundEffectsVolume * Volume;

            if (state is null)
            {
                return;
            }
            MediaPlayer.IsRepeating = true;

            if (state is FirstLevel)
            {
                if (MediaPlayer.Queue.ActiveSong != songs["beginning"])                
                    MediaPlayer.Play(songs["beginning"]);                
            }
            else if (state is SynphState)
            {
                if (MediaPlayer.Queue.ActiveSong != songs["owls"])
                    MediaPlayer.Play(songs["owls"]);
            }
            else
            {
                MediaPlayer.Stop();
            }
        }
        private static void Changed(object sender, EventArgs e)
        {
            if (state is FirstLevel)
            {
                if (MediaPlayer.Queue.ActiveSong != songs["beginning"])
                {
                    MediaPlayer.Play(songs["beginning"]);
                }
            }
            else if (state is SynphState)
            {
                if (MediaPlayer.Queue.ActiveSong != songs["owls"])
                    MediaPlayer.Play(songs["owls"]);
            }
            else
            {
                MediaPlayer.Stop();
            }
        }
    }
}
