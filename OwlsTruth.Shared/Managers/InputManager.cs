﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace OwlsTruth
{
    public static class InputManager
    {
        public static KeyboardState currentKeyboardState = new KeyboardState();
        public static KeyboardState lastKeyboardState = new KeyboardState();

        public static bool Up => (IsNewKeyPress(Keys.W) || IsNewKeyPress(Keys.Up)) || (IsNewKeyPress(Keys.Space));
        public static bool Left => (IsKeyDown(Keys.A) || IsKeyDown(Keys.Left));
        public static bool Right => (IsKeyDown(Keys.D) || IsKeyDown(Keys.Right));

        public static void Update()
        {
            lastKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();            
        }
        
        public static bool IsNewKeyPress(Keys key)
        {
            return (currentKeyboardState.IsKeyDown(key) &&
                lastKeyboardState.IsKeyUp(key));
        }

        public static bool IsKeyDown(Keys key)
        { return (currentKeyboardState.IsKeyDown(key)); }

        public static bool IsNewKeyRelease(Keys key)
        {
            return (lastKeyboardState.IsKeyDown(key) &&
                currentKeyboardState.IsKeyUp(key));
        }
    }
}
