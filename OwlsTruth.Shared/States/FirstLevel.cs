﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace OwlsTruth
{
    public class FirstLevel : GameState
    {
        public FirstLevel(OwlsTruth game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            parallaxSpeed = new Vector2(0.9f, 0.94f);
        }

        public override void Load(ContentManager content)
        {
            base.Load(content);

            var flour = new Sprite(_content.Load<Texture2D>("Sprites/lvl1/flour"))
            {
                Position = new Vector2(-300, 100)
            };
            var wall = new Sprite(_content.Load<Texture2D>("Sprites/lvl1/wall"))
            {
                Position = new Vector2(-268, 20)
            };
            var trees = new Sprite(_content.Load<Texture2D>("Sprites/lvl1/trees"))
            {
                Position = new Vector2(-56, 21)
            };

            var door = new Door(_content.Load<Texture2D>("Sprites/lvl1/doorOpen"),
                 () => { _game.ChangeState(new SynphState(_game, _graphicsDevice)); },
                _content.Load<Texture2D>("Sprites/lvl1/door"),
                new Animation(_content.Load<Texture2D>("Sprites/lvl1/doorOpening"), 4) { IsLooping =false,Speed=0.3f })
            {
                Position = new Vector2(-170,68)
            };

            SceneBox = new Rectangle(-300, -1000, 432, 2000);

            parallaxlayer = new List<Sprite>()
            {
                trees
            };

            backlayer = new List<Sprite>()
            {
                wall
            };

            mainlayer = new List<Sprite>()
            {
                flour,
                player
            };
            doors = new List<Door>()
            {
                door
            };
            Camera2D.Zoom = 3f;
            player.SceneBox = SceneBox;
            OwlsTruth.Color = new Color(180, 200, 230);
            IsLoaded = true;
            
        }
    }
}
