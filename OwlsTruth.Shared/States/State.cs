﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace OwlsTruth
{
    public abstract class State
    {
        protected GraphicsDevice _graphicsDevice;
        protected OwlsTruth _game;        
        protected ContentManager _content;

        public bool IsLoaded;
        public float slide;
        public int elementsLoaded;
        public int elements;

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
        public abstract void Update(GameTime gameTime);
        public abstract void Load(ContentManager content);
        public abstract void UnLoad();

        public State(OwlsTruth game, GraphicsDevice graphicsDevice)
        {
            _game = game; _graphicsDevice = graphicsDevice;
            elementsLoaded = 0;
            elements = 0;
        }
    }
}
