﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace OwlsTruth
{
    class SynphState : GameState
    {
        private Rectangle Ending;
        private bool ending;
        private float timer;

        public SynphState(OwlsTruth game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            parallaxSpeed = new Vector2(0.8f, 0.8f);
            IsLoaded = false;
            ending = false;
            timer = 0;
        }

        public override void Load(ContentManager content)
        {
            base.Load(content);            

            var background = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/background"))
            {
                Position = new Vector2(-600, -160)
            };

            var Bottom1 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Bottom1"))
            {
                Position = new Vector2(-50, 56)
            };            
            var Bottom2 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Bottom2"))
            {
                Position = new Vector2(-340, 6)
            };
            var Bottom3 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Bottom3"))
            {
                Position = new Vector2(-614, - 14)
            };
            var Bottom4 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Bottom4"))
            {
                Position = new Vector2(-600, -800)
            };
            var Bottom5 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Bottom5"))
            {
                Position = new Vector2(-600, -800)
            };
            var Bottom6 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Bottom6"))
            {
                Position = new Vector2(-600, -800)
            };

            var Top1 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Top1"))
            {
                Position = new Vector2(-50, -47)
            };
            var Top2 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Top2"))
            {
                Position = new Vector2(-340, -71)
            };
            var Top3 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Top3"))
            {
                Position = new Vector2(-614, -187)
            };
            var Top4 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Top4"))
            {
                Position = new Vector2(-600, -800)
            };
            var Top5 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Top5"))
            {
                Position = new Vector2(-600, -800)
            };

            var Middle3 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Middle3"))
            {
                Position = new Vector2(-614, -110)
            };

            var flour1 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/flour1"))
            {
                Position = new Vector2(-50, 40)
            };
            var flour2 = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/flour2"))
            {
                Position = new Vector2(-340, -10)
            };
            var flour3Tex = _content.Load<Texture2D>("Sprites/lvl2/flour3");
            var flour31 = new Sprite(flour3Tex)
            {
                Position = new Vector2(-614, -30)
            };
            var flour32 = new Sprite(flour3Tex)
            {
                Position = new Vector2(-614, -126)
            };
            var flour33 = new Sprite(flour3Tex)
            {
                Position = new Vector2(-600, -800)
            };
            var flour34 = new Sprite(flour3Tex)
            {
                Position = new Vector2(-600, -800)
            };
            var flour35 = new Sprite(flour3Tex)
            {
                Position = new Vector2(-600, -800)
            };

            var Sova = new Sprite(_content.Load<Texture2D>("Sprites/lvl2/Sova"))
            {
                Position = new Vector2(-310, -51)
            };

            var tileTex = _content.Load<Texture2D>("Sprites/lvl2/tile");

            var tile1 = new Tile(tileTex,3)
            {
                offset = 0,
                Position = new Vector2(-92, 30)
            };
            var tile2 = new Tile(tileTex,3)
            {
                offset = 1,
                Position = new Vector2(-134, 20)
            };
            var tile3 = new Tile(tileTex,3)
            {
                offset = 2,
                Position = new Vector2(-176, 10)
            };
            var tile4 = new Tile(tileTex,3)
            {
                offset = 3,
                Position = new Vector2(-218, 0)
            };

            Ending = Sova.Rectangle;
            SceneBox = new Rectangle(-2000, -1000, 4000, 2000);

            var door = new Door(_content.Load<Texture2D>("Sprites/lvl1/doorOpen"),
                 () => { _game.ChangeState(new FirstLevel(_game, _graphicsDevice) { playerStartPos = new Vector2(-164,20)}); },
                _content.Load<Texture2D>("Sprites/lvl1/door"),
                new Animation(_content.Load<Texture2D>("Sprites/lvl1/doorOpening"), 4) { IsLooping = false, Speed = 0.3f })
            {
                Position = new Vector2(-2, 8),
                isOpened = true
            };

            parallaxlayer = new List<Sprite>()
            {
                background
            };

            backlayer = new List<Sprite>()
            {
                Top1,
                Top2,
                Top3,
                Top4,
                Top5,
                Middle3,
                Bottom1,
                Bottom2,
                Bottom3,
                Bottom4,
                Bottom5,
                Bottom6,
                Sova
            };

            mainlayer = new List<Sprite>()
            {
                flour1,
                flour2,
                flour31,
                flour32,
                flour33,
                flour34,
                flour35,
                tile1,
                tile2,
                tile3,
                tile4,
                player
            };
            doors = new List<Door>()
            {
                door
            };
            Camera2D.Zoom = 3f;
            player.SceneBox = SceneBox;
            OwlsTruth.Color = new Color(6, 2, 8);
            IsLoaded = true;
        }

        public override void Update(GameTime gameTime)
        {
            foreach(var tile in mainlayer)
            {
                if(tile is Tile)
                {
                    ((Tile)tile).PlaySound(player.Origin);
                }
            }            
            base.Update(gameTime);
            if (player.Rectangle.Intersects(Ending) || ending)
            {
                if (timer >= 3)
                {
                    _game.Exit();
                }
                ending = true;
                player.Velocity.Y = -0.03f;
                player.Velocity.X = 0;
                player.isControllable = false;
                OwlsTruth.Color = new Color(timer * 100, timer * 100, timer * 100, timer / 3);
                SoundManager.MusicVolume = 0;
                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
        }
    }
}
