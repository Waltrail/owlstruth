﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace OwlsTruth
{ 
    class IntroState : State
    {
        private bool isStart;

        public IntroState(OwlsTruth game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            Camera2D.Zoom = 3;
            slide = 0;
            isStart = false;
            OwlsTruth.Color = new Color(0, 20, 50); // new Color(180, 200, 230);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(transformMatrix: Camera2D.Transform);
            spriteBatch.DrawString(OwlsTruth.font, "Enter to continue", new Vector2(), Color.Wheat,0, new Vector2(110,40),0.2f, SpriteEffects.None, 0);


            spriteBatch.End();
        }

        public override void Load(ContentManager content)
        {
            IsLoaded = true;
        }

        public override void UnLoad()
        {
            
        }

        public override void Update(GameTime gameTime)
        {
            Camera2D.Follow(new Vector2());
            Camera2D.Update(gameTime);
            if (InputManager.IsNewKeyPress(Microsoft.Xna.Framework.Input.Keys.Enter))
            {
                isStart = true;
            }
            if (isStart)
            {
                Console.WriteLine(slide);
                if (slide >= 180)
                {
                    _game.ChangeState(new FirstLevel(_game, _graphicsDevice));
                }
                OwlsTruth.Color = new Color((int)slide, (int)slide + 20, (int)slide + 50);
                slide += (float)gameTime.ElapsedGameTime.TotalMilliseconds/16;
            }
        }
    }
}
