﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace OwlsTruth
{
    public class GameState : State
    {
        public Player player;
        public Vector2 playerStartPos;
        public List<Sprite> backlayer;
        public List<Sprite> mainlayer;
        public List<Sprite> frontlayer;
        public List<Sprite> parallaxlayer;
        public List<Door> doors;

        protected Rectangle SceneBox;
        protected Vector2 parallaxSpeed;

        public GameState(OwlsTruth game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            _game = game;
            _graphicsDevice = graphicsDevice;
            playerStartPos = new Vector2();
            IsLoaded = false;
            SceneBox = new Rectangle();

            Camera2D.DefaultVelosity = 0.2f;
            Camera2D.VelosityBoost = 0.05f;
            Camera2D.VelosityBound = 0.6f;                  
        }
        public override void Load(ContentManager content)
        {
            _content = content;

            Camera2D.CameraPosition = new Vector2(0, -500);
            var OwlRun = new Animation(_content.Load<Texture2D>("Sprites/Player/owlRun"), 6) { Speed = 0.3f };
            var OwlIdle = new Animation(_content.Load<Texture2D>("Sprites/Player/owlIdle"), 3) { Speed = 0.3f };
            var OwlRare = new Animation(_content.Load<Texture2D>("Sprites/Player/owlRare"), 3) { IsLooping = false, Speed = 0.3f };
            var OwlJump = new Animation(_content.Load<Texture2D>("Sprites/Player/owlJump"), 3) { IsLooping = false, Speed = 0.3f };
            var OwlAir = new Animation(_content.Load<Texture2D>("Sprites/Player/owlAir"), 3) { Speed = 0.3f };

            var PlayerAnimations = new Dictionary<string, Animation>()
            {
                { "run" ,OwlRun },
                { "jump", OwlJump },
                { "air" , OwlAir },
                { "idle", OwlIdle },
                { "rare", OwlRare }
            };
            player = new Player(PlayerAnimations) { Position = playerStartPos, BoxFix = 0, SceneBox = SceneBox };

            parallaxlayer = new List<Sprite>();
            backlayer = new List<Sprite>();
            mainlayer = new List<Sprite>();            
            frontlayer = new List<Sprite>();
            doors = new List<Door>();
        }

        public override void Update(GameTime gameTime)
        {
            worldPosition = Camera2D.CameraPosition;
            if (!IsLoaded)
            {
                return;
            }
            if (slide <= 10)
            {
                slide += (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            SoundManager.MusicVolume = (float)Math.Round(SoundManager.Volume * slide / 10, 2);    

            foreach (var s in backlayer)
            {
                s.Update(gameTime, backlayer);
            }

            foreach (var s in parallaxlayer)
            {
                s.Update(gameTime, parallaxlayer);
            }
            foreach (var door in doors)
            {
                door.Update(gameTime, mainlayer);
            }
            foreach (var s in mainlayer)
            {
                s.Update(gameTime, mainlayer);
            }            
            foreach (var s in frontlayer)
            {
                s.Update(gameTime, frontlayer);
            }
            Camera2D.Follow(player);
            Camera2D.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsLoaded)
            {
                return;
            }            
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.GetViewMatrix(parallaxSpeed));
            foreach (var s in parallaxlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Camera2D.Transform);
            foreach (var s in backlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            foreach (var door in doors)
            {
                door.Draw(gameTime, spriteBatch);
            }
            foreach (var s in mainlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            foreach (var s in frontlayer)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();            
        }

        public override void UnLoad()
        {
            
        }
    }
}
